#! /usr/bin/python3
import pandas as pd
import argparse, re, os.path
import numpy as np
import plotly.graph_objs as go
from sys import exit
from plotly.offline import plot
from sklearn import linear_model

# Extract data from existing files
headerNames =  ['taskId', 'submitTime', 'waitTime', 'execStartTime', 'execFinTime', 'execTime']
xlabel = 'Available Power (Watts)'
ylabel = 'Execution time (Seconds)'
def setup_args():
  parser = argparse.ArgumentParser(description='Plot available power vs execution times')
  parser.add_argument('-s', '--save', help='Name of file to save in', required=False)
  parser.add_argument('-f','--files', nargs='+', help='Names of log files', required=True)
  parser.add_argument('-d', '--Def',help='Path to the JSON file')
  parser.add_argument('-p', '--percIncExectime', help='Acceptable percentage increase in execution time [0,1]')
  parser.add_argument('-b', '--base', help='Path to directory contaning baselines')
  return parser.parse_args()

def gen_bar(df):
  cols = df.columns.values
  return go.Bar(
    x = df[cols[1]],
    y = df[cols[-1]]
  )

def gen_scatter(df):
  cols = df.columns.values
  tf = df.sort_values(by=[cols[1]])
  cols = tf.columns.values
  return go.Scatter(
      x = tf[cols[1]],
      y = tf[cols[-1]]
  )

def gen_marking_line(df, acceptable_extime, name):
  cols = df.columns.values
  x = [f for f in range(0, 100)]
  y = [acceptable_extime for f in range(0, 100)]
  return go.Scatter(
    x = x,
    y = y,
    name=name
  )
  
def gen_data(directory, filename):
  return len(pd.read_csv(directory+filename,parse_dates=True))-2

def display_or_save(fig, save):
  if save:
    plot(fig, image = 'png', image_filename=save)
  else:
    plot(fig)

def gen_layout(xlabel, ylabel, title):
  return go.Layout(
    xaxis = dict(
      title=xlabel,
      tickmode='linear',
      tick0=0,
      dtick=5,
      ticklen=5,
      tickwidth=2
      ),
    yaxis = dict(title=ylabel),
    title=title
  )

"""
  Calculating approx_extime using baselines data
"""
def compute_tolerance(args, df):
  if not args.Def or not args.percIncExectime or not args.base:
    return None
  runs_df = [gen_data(args.base, f) for f in os.listdir(args.base)]
  approx_extime = np.mean(np.array(runs_df))
  task = pd.read_json(args.Def).iloc[0]
  cols = df.columns.values
  extimes = df[cols[-1]]
  avail_pwr = df[cols[1]]
  #approx_extime = np.mean(np.array(clean_extimes))
  acceptable_extime = approx_extime*(1.0+float(args.percIncExectime))
  model = gen_model(extimes, avail_pwr)
  predicted_pwr = model.predict([[acceptable_extime]])[0]
  print('Data used to train model:')
  print(df)
  print('Approx Execution time from baselines: ' + str(approx_extime))
  print('Acceptable execution time: ' + str(acceptable_extime))
  tolerance = 0.0 if predicted_pwr >= task.watts else 1-predicted_pwr/task.watts
  return [approx_extime, tolerance]

def gen_model(exectimes, avail_pwr):
  df = pd.DataFrame({'exec_time': exectimes, 'avail_pwr': avail_pwr}, columns=['exec_time', 'avail_pwr'])
  model = linear_model.LinearRegression()
  model.fit(df[['exec_time']], df.avail_pwr)
  return model

def gen_all_acceptable_times(aprx_extime):
  all_times = []
  for i in [float(j) / 10 for j in range(0, 11, 1)]:
    acceptable = aprx_extime*(1.0+i)
    all_times.append(acceptable)
  return all_times

def plotAll(args, headerNames):
  dfs = [getCustomDataFrame(f, headerNames) for f in args.files]
  dfs = [df for df in dfs if df is not None]
  if len(dfs) < 1:
    print('No data to plot. Exiting...')
    return
  customHeaders = ['Benchmark Name', 'Power (Watts)', 'MeanExecutionTime (seconds)']
  dataframe = pd.DataFrame(dfs, columns=customHeaders)
  benchmarkName = dataframe[customHeaders[0]][0]
  title = benchmarkName+'_AvailablePower-vs-ExecutionTime'
  [approx_extime, tolerance] = compute_tolerance(args, dataframe)
  if not tolerance is None:
    print('Predicted tolerance in Watts: ' + str(tolerance))
  all_times = gen_all_acceptable_times(approx_extime)
  layout = gen_layout(xlabel, ylabel, title)
  data = [gen_bar(dataframe), gen_scatter(dataframe)]
  for i, time in enumerate(all_times):
    data.append(gen_marking_line(dataframe, time, str(i/10)))
  fig = go.Figure(data, layout=layout)
  display_or_save(fig, args.save)

'''
  @param: String fileName, list(String) headerNames
  @return: None if file is not valid
           List of values in the following order
           [BenchmarkName, Power, MeanExecutionTime]
'''
def getCustomDataFrame(fileName, headerNames):
  [valid, matchObj] = validate(fileName)
  if not valid:
    return None
  df = pd.read_csv(fileName, skiprows=1, sep=',', names = headerNames)
  meanExecutionTime = df[headerNames[-1]].mean()
  hostname = matchObj.group(1)
  benchmarkName = matchObj.group(2)
  power = float(matchObj.group(3))
  return [benchmarkName, power, meanExecutionTime]

def validate(fileName):
  matchObj = re.match(r'^Electron-(.*)-Run\d*-(.*)-AvailPower(.*)_(.*)_runtimeMetrics.log$', fileName.strip())
  if (not matchObj or len(matchObj.groups()) != 4):
    print('Incorrect filename, format: Electron-<host>-Run<x>-<benchmark name>-AvailPower<p>_<timestamp>_runtimeMetrics.log')
    print('Skipping file: ' + fileName)
    return [False, None]
  exists = os.path.isfile(fileName)
  if not exists:
    print('File: ' + fileName + ' does not exist, skipping...')
  return [exists, matchObj]


if __name__ == '__main__':
  args = setup_args()
  plotAll(args, headerNames)
