# Watts Tolerance

## Instructions to run:

python3 script.py [-h] [-s SAVE] -f FILES [FILES ...]

Plot available power vs execution times

optional arguments:
  -h, --help            show this help message and exit
  -s SAVE, --save SAVE  Name of file to save in

required arguments:
  -f FILES [FILES ...], --files FILES [FILES ...]
                        Names of log files


## Description:

Each file name should match the given format:

Electron-<hostname>-Run<x>-<benchmark name>-AvailPower<p>\_<timestamp>\_runtimeMetrics.log

Given a set of files containing execution times for a given amount of power, the script plots a single bar graph for the given benchmark representing variation in execution time with the amount of available power.

* X axis: Available power in Watts
* Y axis: Execution time in seconds.
* Title: <benchmark name>\_AvailablePower-vs-ExecutionTime
